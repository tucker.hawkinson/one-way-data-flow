import axios from 'axios';

const state = {
  posts: [
    {
      userId: 1,
      id: 1,
      title: 'Some Title',
      body: 'Some Body',
    },
    {
      userId: 1,
      id: 2,
      title: 'Some Title 2',
      body: 'Some Body 2',
    },
  ],
};

const getters = {
  getAllPosts(state) {
    return state.posts;
  },
};

const mutations = {
  updatePosts(state, updatedPosts) {
    state.posts = updatedPosts;
  },
};

const actions = {
  async updatePosts({ commit }) {
    try {
      const posts = await axios.get(
        'https://jsonplaceholder.typicode.com/posts',
      );

      commit('updatePosts', posts.data);
    } catch (err) {
      console.log('Handle ERROR Here');
    }
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
